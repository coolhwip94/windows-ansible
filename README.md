# Windows with Ansible
> Repo with example for working with Ansible as control node and a Windows Machine as the target node.

## Setup
---
> What this lab example consists of
- `Windows Lab VM` (target)
  - OpenSSH Installed
- `Macbook` (control)
  - Ansible Host
- `host_vars/win.yml`
  - file containing variables including `ansible_password` which is protected by vault

## Running
---
- To run playbook example
```
ansible-playbook file_example.yml
```
### Tasks
- `name: "Task 1 : Add Line to file"`
  - Simple tasks that adds a line to a .txt file

- `copy_generated_template.yml` : this playbook generates and example config file and copies it to the remote host
```
ansible-playbook copy_generated_template.yml
```

## References
---
- https://docs.ansible.com/ansible/2.9/modules/list_of_windows_modules.html
- https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse
